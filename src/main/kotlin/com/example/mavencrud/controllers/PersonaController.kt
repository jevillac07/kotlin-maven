package com.example.mavencrud.controllers

import com.example.mavencrud.exceptions.BusinessException
import com.example.mavencrud.exceptions.NotFoundException
import com.example.mavencrud.models.Persona
import com.example.mavencrud.services.PersonaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/v1/persona")
class PersonaController {
    @Autowired
    val personaService: PersonaService? = null

    @GetMapping
    fun get(): ResponseEntity<List<Persona>> {
        return try {
            ResponseEntity(personaService!!.list(), HttpStatus.OK)
        } catch (e: Exception) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("/{id}")
    fun get(@PathVariable("id") id: Long): ResponseEntity<Persona> {
        return try {
            ResponseEntity(personaService!!.load(id), HttpStatus.OK)
        } catch (e: BusinessException) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        } catch (e: NotFoundException) {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping
    fun post(@RequestBody persona: Persona): ResponseEntity<Persona> {
        return try {
            personaService!!.save(persona)
            ResponseEntity(HttpStatus.CREATED)
        } catch (e: BusinessException) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PutMapping
    fun putPersona(@RequestBody persona: Persona): ResponseEntity<Persona> {
        return try {
            personaService!!.save(persona)
            ResponseEntity(HttpStatus.OK)
        } catch (e: BusinessException) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: Long): ResponseEntity<Any> {
        return try {
            personaService!!.delete(id)
            ResponseEntity(HttpStatus.OK)
        } catch (e: BusinessException) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        } catch (e: NotFoundException) {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }
}