package com.example.mavencrud.services

import com.example.mavencrud.models.Persona

interface PersonaService {
    fun list(): List<Persona>
    fun load(id: Long): Persona
    fun save(persona: Persona): Persona
    fun update(persona: Persona): Persona
    fun delete(id: Long)
}