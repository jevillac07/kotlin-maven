package com.example.mavencrud.services

import com.example.mavencrud.exceptions.BusinessException
import com.example.mavencrud.exceptions.NotFoundException
import com.example.mavencrud.repositories.PersonaRepository
import com.example.mavencrud.models.Persona
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.jvm.Throws

@Service
class PersonaServiceImpl: PersonaService {
    @Autowired
    val personaRepository: PersonaRepository? = null

    @Throws(BusinessException::class)
    override fun list(): List<Persona> {
        try {
            return personaRepository!!.findAll()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    @Throws(BusinessException::class, NotFoundException::class)
    override fun load(id: Long): Persona {
        val op: Optional<Persona>

        try {
            op = personaRepository!!.findById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }

        if (op.isEmpty) {
            throw NotFoundException("Not found person by id: $id")
        }

        return op.get()
    }

    @Throws(BusinessException::class)
    override fun save(persona: Persona): Persona {
        try {
            return personaRepository!!.save(persona)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    @Throws(BusinessException::class)
    override fun update(persona: Persona): Persona {
        try {
            return personaRepository!!.save(persona)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    @Throws(BusinessException::class, NotFoundException::class)
    override fun delete(id: Long) {
        val op: Optional<Persona>;

        try {
            op = personaRepository!!.findById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }

        if (op.isEmpty) {
            throw NotFoundException("Not found person by id: $id")
        } else {
            try {
                //personaRepository!!.delete(op.get())
                personaRepository!!.deleteById(id)
            } catch (e: Exception) {
                throw BusinessException(e.message)
            }
        }
    }
}