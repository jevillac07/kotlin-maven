package com.example.mavencrud.exceptions

class BusinessException(message: String?): Exception(message) {
}