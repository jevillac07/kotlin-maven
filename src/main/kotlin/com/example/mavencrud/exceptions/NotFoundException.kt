package com.example.mavencrud.exceptions

class NotFoundException(message: String?): Exception(message) {
}