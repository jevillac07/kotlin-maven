package com.example.mavencrud.models

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "persona")
data class Persona(
    val dni: Long = 0, val name: String = "",
    val lastName: String = "", val birthDate: String = "") {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0;
}