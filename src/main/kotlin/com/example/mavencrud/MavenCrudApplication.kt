package com.example.mavencrud

import com.example.mavencrud.models.Persona
import com.example.mavencrud.repositories.PersonaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MavenCrudApplication: CommandLineRunner {
	@Autowired
	val personaRepository: PersonaRepository? = null

	override fun run(vararg args: String?) {

		val persona = Persona(46073831, "Juan", "Villa", "04/04/1989")
		personaRepository!!.save(persona)
	}

}

fun main(args: Array<String>) {
	runApplication<MavenCrudApplication>(*args)
}
